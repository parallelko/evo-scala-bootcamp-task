## Introduce
Algorithm for comparing the strength of Texas Hold'em Hands.
This Algorithm was developed as part of test task for Evolution Gaming Scala bootcamp

### Program Language
Python 3.5+

### Setup
For Python installation please proceed to https://www.python.org/.

Algorithm do not use external libs for python so it could run in any environment.


### How to use
As if Algorithm receive data from stdin, the best way to provide data is read it from file and pass it to stdin.
```bash
cat <input_file> | python3 main.py > <output_file>
```
### Benefits
Algorithm also compares kickers (highest card) in case of same combinations.

For example:
```
4cKs4h8s7s 7hAs 7d6s
```
Both hands got combinations with same two pairs but 7hAs got Ace as kicker and will be stronger than 7d6s
#### Tests
Also was implemented some tests
```
python3 tests.py
```
