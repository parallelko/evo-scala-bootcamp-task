from collections import Counter
from dataclasses import dataclass, field
from utils import window
from textwrap import wrap


def sort_rank_list(cards: list) -> list:
    cards_ranks = [card.rank for card in cards]
    cards_ranks.sort(reverse=True)
    return cards_ranks


def compare_sums(seq: list) -> int:
    arithmetic_sum = (2*min(seq) + (len(seq)-1))/2*len(seq)
    result = arithmetic_sum == sum(seq)
    return max(seq) * result


def check_stright(all_cards):
    if len(all_cards) < 5:
        return 0
    cards_ranks = sort_rank_list(all_cards)
    clear_ranks = list(dict.fromkeys(cards_ranks))
    clear_ranks_ace_1 = list(map(lambda x: 1 if x == 14 else x, clear_ranks))
    clear_ranks_ace_1.sort(reverse=True)

    stright_direct = max(list(map(
            lambda seq: compare_sums(seq),
            window(clear_ranks, 5)
            )
    )) if len(clear_ranks) >= 5 else 0

    stright_ace_1 = max(list(
        map(
            lambda seq: compare_sums(seq),
            window(clear_ranks_ace_1, 5)
            )
    )) if len(clear_ranks_ace_1) >= 5 else 0

    return max(stright_direct, stright_ace_1)


def max_of_case(collection: dict, case: int) -> int:
    res = dict(filter(lambda items: items[1] == case, collection.items()))
    return max(res.keys()) if len(res) > 0 else 0


def result_sum_calculation(degrees_list: list, base=15) -> int:
    return sum([
        base**(len(degrees_list)-idx-1)*el
        for idx, el in enumerate(degrees_list)])


@dataclass
class Card:
    card_str: str
    rank: int = field(init=False)
    suit: str = field(init=False)

    def __post_init__(self):
        if len(self.card_str) != 2:
            raise ValueError(f'{self.card_str} - incomplete card input')
        rank_map = {'A': 14, 'K': 13, 'Q': 12, 'J': 11, 'T': 10,
                    '9': 9, '8': 8, '7': 7, '6': 6, '5': 5, '4': 4,
                    '3': 3, '2': 2}
        suits = ['h', 'd', 'c', 's']
        rank = rank_map.get(self.card_str[0])
        if not rank:
            raise ValueError(f'{self.card_str} - wrong card rank input')
        suit = self.card_str[1]
        if suit not in suits:
            raise ValueError(f'{suit} - wrong suit input')
        self.rank = rank
        self.suit = suit


class PokerHand:
    def __init__(self, card_string):
        self.cards_list = self._validate_string(card_string)
        if len(self.cards_list) != 2:
            raise ValueError(f'{card_string} - Wrong number of hand cards')
        self.card_string = card_string

    @staticmethod
    def _validate_string(cards_string: str) -> list:
        if len(cards_string) % 2 != 0:
            raise ValueError(f'{cards_string} - incomplete card input')
        return [Card(item) for item in wrap(cards_string, 2)]

    @staticmethod
    def suits_count(cards: list) -> dict:
        return dict(Counter([card.suit for card in cards]))

    @staticmethod
    def straight_flush_result(cards, flush_key):
        suit_cards = [card for card in cards if card.suit == flush_key]
        return check_stright(suit_cards)

    @staticmethod
    def four_of_kind_result(collection):
        res = max_of_case(collection, 4)
        if res != 0:
            collection.pop(res)
            deg_list = [res] + sorted(
                collection.keys(), reverse=True)[:1]
            return result_sum_calculation(deg_list)
        else:
            return 0

    @staticmethod
    def full_house_result(collection):
        high = max_of_case(collection, 3)
        collection.pop(high)
        low = max_of_case(collection, 3)
        if low == 0:
            low = max_of_case(collection, 2)
        if high > 0 and low > 0:
            return 15*high+low
        else:
            return 0

    @staticmethod
    def flush_result(cards, flush_key):
        lst = [card.rank for card in cards if card.suit == flush_key]
        lst.sort(reverse=True)
        return result_sum_calculation(lst[:5], 15)

    @staticmethod
    def three_of_kind_result(collection):
        res = max_of_case(collection, 3)
        if res != 0:
            collection.pop(res)
            deg_list = [res] + sorted(
                collection.keys(), reverse=True)[:2]
            return result_sum_calculation(deg_list)
        else:
            return 0

    @staticmethod
    def two_pairs_result(collection):
        pairs_keys = list(dict(filter(
            lambda items: items[1] == 2, collection.items())).keys())
        if len(pairs_keys) < 2:
            return 0
        pairs_keys.sort(reverse=True)
        high = pairs_keys[0]
        low = pairs_keys[1]
        collection.pop(high)
        collection.pop(low)
        kicker = max(collection.keys())
        return result_sum_calculation([high, low, kicker])

    @staticmethod
    def one_pair_result(collection):
        res = max_of_case(collection, 2)
        if res != 0:
            collection.pop(res)
            deg_list = [res] + sorted(
                collection.keys(), reverse=True)[:3]
            return result_sum_calculation(deg_list)
        else:
            return 0

    @staticmethod
    def kicker_calc(collection):
        res = sorted(collection.keys(), reverse=True)[:5]
        return result_sum_calculation(res, 15)

    def get_power(self, deck_cards):
        all_cards = self.cards_list + deck_cards
        suits = self.suits_count(all_cards)

        have_flash = dict(
            filter(lambda elem: elem[1] >= 5, suits.items()))

        flush_suit = max(have_flash.keys()) if len(have_flash) > 0 else ""

        straight_power = check_stright(all_cards)

        cards_ranks = [card.rank for card in all_cards]
        collect = Counter(cards_ranks)

        # Check for straight Flush
        if straight_power > 0 and flush_suit != '':
            result = self.straight_flush_result(all_cards, flush_suit)
            if result > 0:
                return (2*15**5 + 15**4 + 2 * 15**3 + 2 * 15**2 + 1 * 15) + result

        # Check for Four of Kind
        result = self.four_of_kind_result(collect)
        if result > 0:
            return (2*15**5 + 15**4 + 2 * 15**3 + 15**2 + 1 * 15) + result

        # Check for Full House
        result = self.full_house_result(collect)
        if result > 0:
            return (2*15**5 + 15**4 + 2 * 15**3 + 1 * 15) + result

        # Check for flush
        if flush_suit != '':
            result = self.flush_result(all_cards, flush_suit)
            return (15**5 + 15**4 + 2 * 15**3 + 1 * 15) + result

        # Check for Straight
        if straight_power > 0:
            return (15**5 + 15**4 + 2 * 15**3) + straight_power

        # Check for three of kind
        result = self.three_of_kind_result(collect)
        if result > 0:
            return (15**5 + 15**4 + 15**3) + result

        # Check for two pairs
        result = self.two_pairs_result(collect)
        if result > 0:
            return (15**5 + 15**4) + result

        result = self.one_pair_result(collect)
        if result > 0:
            return 15**5 + result

        return self.kicker_calc(collect)