import sys
from hands import PokerHand, Card
from textwrap import wrap


def input_and_validate(cards_string):
    all_cards = cards_string.replace(' ', '')
    if len(all_cards) % 2 != 0:
        print('Incomplete input')
        return None, None

    cards_list = wrap(all_cards, 2)

    # Check for duplicates
    if len(cards_list) != len(list(dict.fromkeys(cards_list))):
        print('Cards string got duplicates')
        return None, None

    deck_hands_cards = cards_string.split(' ')
    if len(deck_hands_cards) < 3:
        print('Not enough hands')
        return None, None

    deck = deck_hands_cards[0]
    hands = deck_hands_cards[1:]

    if len(deck) != 5 * 2:
        print(f'Incomplete deck input: {deck}')
        return None, None

    try:
        hands_list = [PokerHand(el) for el in hands]
        deck_cards = [Card(el) for el in cards_list[:5]]
    except ValueError as Err:
        print(Err)
        return None, None

    return deck_cards, hands_list


def calculate_hands(cards_string_line):
    deck_cards, hands_list = input_and_validate(cards_string_line)
    if not deck_cards and not hands_list:
        return None

    result_dict = {
        hand.card_string: hand.get_power(deck_cards) for hand in hands_list}

    sorted_dict = {k: v for k, v in sorted(
        result_dict.items(), key=lambda item: item[1])}

    last_value = 0
    output_string = ''
    for key, value in sorted_dict.items():
        if value == last_value:
            output_string += '=' + key
        else:
            output_string += ' ' + key
        last_value = value
    print(output_string.strip())
    return output_string.strip()


def main():
    # Input replace
    for line in sys.stdin:
        calculate_hands(line.strip())


if __name__ == '__main__':
    main()