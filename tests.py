import sys
import unittest
from main import calculate_hands

TEST_DATA = [
    {
        'TITLE': 'Data in task 1',
        'CARDS':'4cKs4h8s7s Ad4s Ac4d As9s KhKd 5d6d',
        'RESULT': 'Ad4s=Ac4d 5d6d As9s KhKd'
    },
    {
        'TITLE': 'Data in task 2',
        'CARDS': '2h3h4h5d8d KdKs 9hJh',
        'RESULT': 'KdKs 9hJh'
    },
    {
        'TITLE': 'Two pairs test',
        'CARDS': '4cKs4h8s7s 7hAs 7d6s',
        'RESULT': '7d6s 7hAs'
    },
    {
        'TITLE': 'Two pairs + pairs with kicker',
        'CARDS': '4cKs4h8s7s Kd4s Kh4d Qh2s Qs3h',
        'RESULT': 'Qh2s=Qs3h Kd4s=Kh4d'
    },
    {
        'TITLE': 'Stright+Flash',
        'CARDS': '3cKs4h8s7s 5d6s 9sAs',
        'RESULT': '5d6s 9sAs'
    },
    {
        'TITLE': 'Full house + Staright',
        'CARDS': 'AcKs4hAsQh AdQd Ah4d JsTs',
        'RESULT': 'JsTs Ah4d AdQd'
    },
    {
        'TITLE': 'Four of kind + Stright + pairs + kickers',
        'CARDS': '2cKs2h6s5d 2d2s 5c7d 6h3s Ah3d QdTd 3h4h',
        'RESULT': 'QdTd Ah3d 5c7d 6h3s 3h4h 2d2s'
    },
    {
        'TITLE': 'Data in task 1',
        'CARDS': '4cKs4h8s7s Ad4s Ac4d As9s KhKd 5d6d',
        'RESULT': 'Ad4s=Ac4d 5d6d As9s KhKd'
    },
    {
        'TITLE': 'Four of kind on deck',
        'CARDS': '4c4s4h4d7s Ad5s KcQd 2s3s 2d7d',
        'RESULT': '2s3s=2d7d KcQd Ad5s'
    },
    {
        'TITLE': 'Three of kind on deck',
        'CARDS': '4c4s8h4d7s Ad9s AcTd 2s3s 2h3d',
        'RESULT': '2s3s=2h3d Ad9s AcTd'
    }
    ]


class CalculateTest(unittest.TestCase):
    def calculate(self, test_data):
        from io import StringIO
        out = StringIO()
        sys.stdout = out
        calculate_hands(test_data['CARDS'])
        output = out.getvalue().strip()
        self.assertEqual(test_data['RESULT'], output)

    def test_calculations(self):
        for data in TEST_DATA:
            self.calculate(data)

    def test_one_hand_input(self):
        from io import StringIO
        out = StringIO()
        sys.stdout = out
        calculate_hands('2h3h4h5d8d KdKs')
        output = out.getvalue().strip()
        self.assertEqual('Not enough hands', output)

    def test_duplicates(self):
        from io import StringIO
        out = StringIO()
        sys.stdout = out
        calculate_hands('2h3h4h5d8d KdKs KdKs')
        output = out.getvalue().strip()
        self.assertEqual('Cards string got duplicates', output)

    def test_not_full_hand(self):
        from io import StringIO
        out = StringIO()
        sys.stdout = out
        calculate_hands('2h3h4h5d8d KdKs Ad')
        output = out.getvalue().strip()
        self.assertEqual('Ad - Wrong number of hand cards', output)


if __name__ == '__main__':
    unittest.main()
